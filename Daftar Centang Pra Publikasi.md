# Daftar Centang Pra Publikasi

- [ ] Judul untuk mesin pencari (maksimal 70 karakter, memuat kata kunci utama atau terkait)
- [ ] Meta deskripsi untuk mesin pencari (maksimal 300 karakter, memuat kata kunci utama atau terkait)
- [ ] Featured Image untuk mesin pencari
- [ ] Alt tag
- [ ] Caption
- [ ] Image atribusi
