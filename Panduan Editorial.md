# Panduan Editorial
Panduan ini berlaku untuk semua konten yang dipublikasikan di dan dari https://techarea.co.id.

Jika ada yang tidak tercakup di sini, konsultasikan dengan Pemimpin Redaksi.

Panduan ini akan diperbarui seiring kebutuhan.

# Daftar Isi

# Staff Editorial
Pemimpin redaksi: TBD

Manajer Konten: TBD

Penyunting Konten: TBD

Kontributor: TBD


# Prinsip
Membuat konten original bermutu tinggi yang berhubungan dengan bisnis Techarea yang banyak dicari orang

# Topik
1. Pembuatan website
2. Pembuatan toko online
3. Pembuatan aplikasi
4. Pengembangan IoT
5. Digital Marketing
6. Topik lain yang terkait dengan topik utama

Hindari pembahasan produk/jasa bisnis lain yang beririsan dengan inti bisnis Techarea

# Tujuan
- Mengedukasi pelanggan potensial

# Gaya
- Berbagi keahlian tanpa menonjolkannya
- Berjualan secara elegan, soft sell
- menulis dengan gaya yang cukup santai, kasual yang akan menyenangkan orang untuk membacanya
- Komprehensif, sangat informatif, terstruktur dengan baik, berkualitas tinggi, tidak mempromosikan diri sendiri

# Target Audiens

# Jenis
- Praktik terbaik, Whitepaper, Studi kasus
- Tutorial, Tatacara
- Panduan
- Artikel berdaftar
- Q & A, FAQ
- Review, Perbandingan
- Newsletter
- Infografis, Cheatsheets
- Buku elektronik

# Format
1. Serial 12.000 kata
2. Bentuk panjang 2.400-3.600 kata
3. Bentuk singkat 1.200-1.800 kata

## Umum
Judul \> Excerpt \> Iktisar (TL;DR) \> Intro \> Table of Content \> Main Content \> Recap \> Kesimpulan \> CTA

## Studi Kasus
Judul \> Excerpt \> Iktisar (TL;DR) \> Intro \> Table of Content \> Client Introduction \> Problem \> Solusi \> Capaian \> Recap \> Kesimpulan

## Tutorial, Tatacara
Judul \> Excerpt \> Iktisar (TL;DR) \> Intro \> Table of Content \> Client Overview \> Problem \> Solusi \> Capaian \> Recap \> Kesimpulan

## Panduan
Judul \> Excerpt \> Iktisar (TL;DR) \> Intro \> Table of Content \> Topic Overview \> Recap \> Kesimpulan

## Q&A, FAQ
Judul \> Excerpt \> Iktisar  (TL;DR) \> Intro \> ToC \> Problem Overview \> Question \> Answer \> Recap \> Kesimpulan

## Review, Perbandingan
Judul \> Excerpt \> Iktisar  (TL;DR) \> Intro \> ToC \> Product Overview \> Product Advantages \> Product Disadvantages \> Recap \> Kesimpulan

# Penulisan Secara Umum
- Ditulis menggunakan bahasa Indonesia yang baik dan benar, referensi ke [PUBEI](https://puebi.readthedocs.io/en/latest/ "PUBEI") , [KBBI](https://kbbi.kemdikbud.go.id/ "KBBI"), [Tesaurus](http://tesaurus.kemdikbud.go.id/ "Tesaurus")
- Ditulis untuk dibaca manusia, dioptimasi untuk mudah ditemukan mesin pencari
- Ditulis menggunakan bahasa tulis, bukan bahasa oral
- Jika terdapat kebimbangan dalam pemilihan kata/frasa asing atau bahasa Indonesia, maka yang dipilih adalah kata/frasa yang sudah banyak diketahui masyarakat umum, lebih populer di internet
- Semua konten tutorial, tatacara harus bisa diproduksi ulang oleh pembaca dan diperbarui agar tetap relevan dengan versi terbaru aplikasi atau perangkat lunak yang digunakan/dimonstrasikan

## Judul dan subjudul
- Kreatiflah dalam menulis judul. Buat yang menarik, tetap faktual. Tulis 6-8 alternatif, minta feedback ke kolega.
- Dilarang menulis judul yang menyesatkan (misleading).
- Dilarang menulis judul yang memancing klik (clickbait) tipe 1, atau yang disebut clicktrap/clicktrick/linktrap. Diperbolehkan menulis judul yang memancing klik (clickbait) tipe 2, atau yang disebut legitclick. Penjelasan clickbait lebih lengkap tonton videonya Veritasium: https://www.youtube.com/watch?v=S2xHZPH5Sng
- Judul dan subjudul ditulis dalam 5-7 kata, 50-60 karakter
- Jangan menggunakan heading terlalu dalam, cukup sampai H3
- Tips: pada beberapa kesempatan menyertakan “Indonesia” atau tahun terkini merupakan ide bagus

## Kalimat
- 1 kalimat tidak boleh lebih dari 12 kata
- 1 paragraf tidak boleh lebih 5 baris
- Menuliskan nomor dalam bentuk Angka, bukan terbilang
- Kebanyakan orang membaca secara skimming saat membaca online. Gunakan kalimat pendek, poin-poin, visual, statistik, dan kutipan menarik untuk membuat pembaca tetap terlibat

## Slug
- Membuat slug yang berguna, ringkas, mudah dibaca, mudah diingat dan dicari
- sebaiknya tidak menyertakan hari, bulan, tahun, kecuali memang perlu

## Kode

# Tautan dan konten tertanam
- Tautan ke luar harus selalu dibuka di tab baru atau jendela baru
- Tautan ke dalam dibuka di tab atau jendela yang sama

# Citra
- Citra harus dihost dan dilayani dari server Techarea
- Menggunakan format citra WebP untuk bitmap dan SVG untuk vektor
- Ukuran berkas citra \<100kb
- Menggunakan ukuran citra yang sesuai
- Dilarang menggunakan citra berhakcipta
- Gunakan citra domain publik, creative common, atau citra yang boleh digunakan secara gratis dengan memberi atribusi ke pemiliknya secara layak.
- Ketika menggunkan citra milik Techarea sendiri, tambahkan tanda air secara elegan tanpa menggangu isi citra tersebut.
- Jika citra memuat informasi, maka tag Alt harus mendekripsikan informasi tersebut.
- Jika citra adalah tautan, maka tag Alt harus berisi tautan tersebut
- Jika citra hanya untuk dekorasi dan tidak memiliki nilai informasi, maka tag alt=“” yang akan digunakan
- Citra hanya digunakan untuk featured image, data, penjelasan teknis langkah demi langkah
- Jangan gunakan citra lebih dari 1 jika hanya dekorasi, cukup gunakan 1 feature image yang representatif

# Tabel
- Jangan menyajikan tabel dengan format citra
- Tabel dengan kolom \<=10 dan/atau baris \<=20 ditulis dalam format html
- Tabel dengan kolom \>10 dan/atau baris \>20 dibuat dengan aplikasi yang mendukung seperti Google Sheet, Airtable dan menanamkannya ke postingan

# Konten dari pranala luar, kutipan dan referensi
- Selalu berikan attribusi yang layak atas karya orang lain

# Komentar
- Terbuka dengan kritik yang membangun
- Membalas, menandai spam komentar yang masuk dalam 24 jam
- Jangan kasih umpan ke pemancing (troll), abaikan. (Cirinya mereka biasanya menginput URL Website mereka untuk tujuan backlink, Jangan Di approve tandai spam saja/delete)

# Penyebaran
## Twiter
- Dalam bentuk utas yang Ringkas, deskriptif
- memanfaatkan #Tagar
- Menggunakan penyingkat URL

## LinkedIn
- Hindari menulis judul berisi daftar, akan terlihat murahan, mudah dilewati dan dilupakan

## Instagram
- Dalam bentuk Image, Video, Bisa Berupa Reels
- Hashtag is the key
- Creative caption, dapat di ambil dari konten
- Location if need it

## Facebook
- Dapat berupa URL, Images, Videos
- Captions & Hashtag untuk memperluas jangkauan 
- Location if need it

# Referensi

# Waktu posting
